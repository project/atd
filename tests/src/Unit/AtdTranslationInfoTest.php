<?php

declare(strict_types=1);

namespace Drupal\Tests\atd\Unit;

use Drupal\atd\AtdTranslationInfo;
use Drupal\Core\Extension\Extension;
use Drupal\Tests\UnitTestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @coversDefaultClass \Drupal\atd\AtdTranslationInfo
 * @group atd
 */
class AtdTranslationInfoTest extends UnitTestCase {

  /**
   * The virtual file system.
   *
   * @var \org\bovigo\vfs\vfsStreamDirectory
   */
  protected vfsStreamDirectory $vfsStreamDirectory;

  /**
   * The extension.
   *
   * @var \Drupal\Core\Extension\Extension|\Prophecy\Prophecy\ObjectProphecy
   */
  protected Extension|ObjectProphecy $extension;

  /**
   * The class that manipulates profile, module and theme's meta information.
   *
   * @var \Drupal\atd\AtdTranslationInfo
   */
  protected AtdTranslationInfo $atdTranslationInfo;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Set up the virtual file system, with basic setup for a 'test_module'.
    $this->vfsStreamDirectory = vfsStream::setup('root', NULL, [
      'test_module' => [
        'test_module.info.yml' => 'name: Test Module',
      ],
    ]);

    // Set up the extension mock.
    $this->extension = $this->prophesize(Extension::class);
    $this->extension->getName()->willReturn('test_module');
    $this->extension->getPath()->willReturn($this->vfsStreamDirectory->url() . '/test_module');

    // Create the AtdTranslationInfo instance that needs testing.
    $this->atdTranslationInfo = new AtdTranslationInfo();
  }

  /**
   * Tests that properties are not added when no translations directory exist.
   *
   * @covers ::systemInfoAlter
   */
  public function testSystemInfoAlterDoesNotAddTranslationPropertiesWhenTranslationDirectoryDoesNotExist(): void {
    $info = [];
    $this->atdTranslationInfo->systemInfoAlter($info, $this->extension->reveal(), 'module');

    // No interface translation properties should be added, as dummy test module
    // does not have a translations directory.
    $this->assertArrayNotHasKey('interface translation project', $info);
    $this->assertArrayNotHasKey('interface translation server pattern', $info);
  }

  /**
   * Tests that properties are added when a translation directory exists.
   *
   * @covers ::systemInfoAlter
   */
  public function testSystemInfoAlterAddsTranslationPropertiesWhenTranslationsDirectoryExists(): void {
    // Create an empty translations directory.
    vfsStream::newDirectory('translations')->at($this->vfsStreamDirectory->getChild('test_module'));

    $info = [];
    $this->atdTranslationInfo->systemInfoAlter($info, $this->extension->reveal(), 'module');

    // Ensure that the interface translation properties are added, with the
    // expected values.
    $this->assertArrayHasKey('interface translation project', $info);
    $this->assertEquals('test_module', $info['interface translation project']);
    $this->assertArrayHasKey('interface translation server pattern', $info);
    $this->assertEquals('vfs://root/test_module/translations/%project.%language.po', $info['interface translation server pattern']);
  }

}
