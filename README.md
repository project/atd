INTRODUCTION
------------

This is a helper module to assist with the translations of custom modules,
themes and profiles.

HOW TO USE
----------

For custom profiles, modules and themes, add a `translations/` directory in the
root directory of the extension; and place the translations in a .po file having
the following pattern: `%project.%language.po`.
Note: don't forget to create an .htaccess file in the `translations/` directory.

Next, rebuild Drupal's caches (i.e. `drush cr`) to ensure the correct meta
information is added for the translated extension. Run `drush locale-check` and
`drush locale-update` to import the translations.

See https://api.drupal.org/api/drupal/core%21modules%21locale%21locale.api.php/group/interface_translation_properties
for more information on the interface translation server pattern.

GENERATING .PO FILES
--------------------

The contributed
[Translation template extractor](https://www.drupal.org/project/potx) can be
used to extract the translation file. After installing the module, visit
`admin/config/regional/translate/extract` and "extract" the translation template
from the profile, module or theme. Rename the downloaded file to the appropriate
pattern (e.g., `example.nl.po`, where example is the machine name of a custom
module) and place it in the extensions `translations/` folder (note: don't
forget to create an .htaccess file in the `translations/` directory).

Note that when you extract the translation templates using the Translation
template extractor module you need to do this separately for every module and
submodule. The `drush locale-check` and `drush locale-update` commands do not
recognize map extractions.
